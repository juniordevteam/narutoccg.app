﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    [Serializable]
    public class User
    {
        public User()
        {
            //this.Cards = new HashSet<ShopCard>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }

        //public virtual ICollection<ShopCard> Cards { get; set; }
    }
}
