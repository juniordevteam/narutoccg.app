﻿using Assets.Scripts.Models;
using System;

[Serializable]
public class ShopCard
{
    public string CardName { get; set; }
    public string CardTagName { get; set; }
    public string CardImagePath { get; set; }
    public byte[] CardImage { get; set; }
    public int Price { get; set; }

    public virtual User User { get; set; }

    public ShopCard() { }
}
