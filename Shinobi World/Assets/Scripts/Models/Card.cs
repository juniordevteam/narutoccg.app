﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts.Models
{
    [Serializable]
    public sealed class Card
    {
        public int Id;
        public string Name;
        public string TagName;
        public string ImagePath;
    }

    [Serializable]
    public class CardWrapper
    {
        public List<Card> Cards;
    }
}
