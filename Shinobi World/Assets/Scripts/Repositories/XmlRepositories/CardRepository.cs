﻿using Assets.Scripts.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace Assets.Scripts.Repositories.XmlRepositories
{
    public class CardRepository : IRepository<List<ShopCard>>
    {
        private XmlSerializer _context = new XmlSerializer(typeof(List<ShopCard>));
        private static String _cardPath = Application.dataPath + @"/Data/XmlData/Shop/Cards.xml";

        public void Add(List<ShopCard> model)
        {
            using (FileStream fs = new FileStream(_cardPath, FileMode.OpenOrCreate, FileAccess.Write))
            {
                _context.Serialize(fs, model);
            }
        }

        public List<ShopCard> GetCards()
        {
            using (FileStream fs = new FileStream(_cardPath, FileMode.Open, FileAccess.Read))
            {
                return (List<ShopCard>)_context.Deserialize(fs);
            }
        }

        public List<ShopCard> GetCardsByUser(int userId)
        {
            return null;
        }
    }
}
