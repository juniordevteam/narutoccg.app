﻿using Assets.Scripts.Models;
using System;
using System.Collections;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour, ICloseble
{
    private string _baseAddress = "http://localhost:58018/";

    public GameObject shopPanel;
    public GameObject shopContent;
    public GameObject buttonPrefab;
    public RectTransform parentPanel;

    public Button closeButton;

    void Start()
    {
        closeButton.onClick.AddListener(CloseWindow);
    }

    public void FillTheShop()
    {
        // Get cards

        var cardListRequest = new WWW(_baseAddress + "api/card");
        StartCoroutine(GetCardList(cardListRequest));
        

    }

    IEnumerator GetCardList(WWW www)
    {
        yield return www;

        if (www.error == null)
        {
            var corectJson = String.Concat("{\"Cards\":", www.text, "}");
            var cardWrapper = JsonUtility.FromJson<CardWrapper>(corectJson);            
            var cardList = cardWrapper.Cards;

            foreach (var card in cardList)
            {
                GameObject newCard = (GameObject)Instantiate(buttonPrefab);

                newCard.name = card.Name;
                newCard.tag = card.TagName;

                newCard.transform.SetParent(parentPanel, false);
                newCard.transform.localScale = new Vector3(1, 1, 1);

                var cardButton = newCard.GetComponent<Button>();

                var imageIcon = cardButton.transform.Find("ImageIcon").gameObject.GetComponent<Image>();
                imageIcon.sprite = AssetDatabase.LoadAssetAtPath<Sprite>(card.ImagePath);

                var buyText = cardButton.transform.Find("PriceText").gameObject.GetComponent<Text>();
                buyText.text = 0 + "$";

                cardButton.onClick.AddListener(Buy);
            }
        }
    }

    public void CloseWindow()
    {
        shopPanel.gameObject.SetActive(false);
        var shopCollection = shopContent.GetComponentsInChildren<Button>();

        foreach (var item in shopCollection)
            Destroy(item.gameObject);
    }

    private void Buy()
    {
        Debug.Log("s");
    }
}
