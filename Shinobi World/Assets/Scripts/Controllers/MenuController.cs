﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public GameObject campaignPanel;
    public GameObject duelPanel;
    public GameObject inventoryPanel;
    public GameObject shopPanel;
    public GameObject achievementsPanel;

    public Button campaignButton;
    public Button duelButton;
    public Button inventoryButton;
    public Button shopButton;
    public Button achievementsButton;
    public Button exitButton;

	void Start ()
    {
        campaignButton.onClick.AddListener(OpenCampaignPanel);
        duelButton.onClick.AddListener(OpenDuelPanel);
        inventoryButton.onClick.AddListener(OpenInventoryPanel);
        shopButton.onClick.AddListener(OpenShopPanel);
        achievementsButton.onClick.AddListener(OpenAchievementsPanel);
        exitButton.onClick.AddListener(ExitGame);
	}

    private void OpenCampaignPanel()
    {
        campaignPanel.SetActive(true);
    }

    private void OpenDuelPanel()
    {
        duelPanel.SetActive(true);
    }

    private void OpenInventoryPanel()
    {
        inventoryPanel.SetActive(true);
        inventoryPanel.GetComponent<InventoryController>().FillTheInventory();

        List<Transform> tabs = new List<Transform>();

        foreach (Transform item in inventoryPanel.transform)
        {
            if (item.CompareTag("InventoryTab"))
                tabs.Add(item);
        }

        foreach (var tab in tabs)
        {
            var tabTitle = tab.transform.Find("TabTitle");
            tabTitle.GetComponent<Button>().onClick.AddListener(delegate { OpenTab(tabs); });
        }
    }

    private void OpenTab(List<Transform> tabs)
    {
        foreach (var tab in tabs)
        {
            var tabContent = tab.Find("TabContent");

            if (!tabContent.gameObject.activeSelf)
                tabContent.gameObject.SetActive(true);
            else
                tabContent.gameObject.SetActive(false);
        }        
    }

    private void OpenShopPanel()
    {
        shopPanel.SetActive(true);
        shopPanel.GetComponent<ShopController>().FillTheShop();
    }

    private void OpenAchievementsPanel()
    {
        achievementsPanel.SetActive(true);
    }

    private void ExitGame()
    {
        Application.Quit();
    }
}
