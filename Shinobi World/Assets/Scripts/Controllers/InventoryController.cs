﻿using UnityEngine;
using UnityEngine.UI;

public class InventoryController : MonoBehaviour, ICloseble
{
    public GameObject inventoryPanel;

    public GameObject cardTab;
    public GameObject equipmentTab;

    public Button closeButton;

    void Start()
    {
        closeButton.onClick.AddListener(CloseWindow);
    }

    public void FillTheInventory()
    {
        Debug.Log("Complete");
    }

    public void CloseWindow()
    {
        inventoryPanel.gameObject.SetActive(false);
    }
}
