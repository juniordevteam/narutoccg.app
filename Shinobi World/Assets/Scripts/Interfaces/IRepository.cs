﻿namespace Assets.Scripts.Interfaces
{
    public interface IRepository<T>
    {
        void Add(T obj);
    }
}
